package com.android.locationtest;

import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;

import androidx.annotation.Nullable;

import com.android.locationtest.interfaces.ServiceCallBacks;

import java.io.OutputStreamWriter;

public class LocationService extends android.app.Service{

    private boolean startToStoreLocation = false;
    private final IBinder binder = new LocationService.LocalBinder();
    private ServiceCallBacks serviceCallbacks;

    public class LocalBinder extends Binder {
        LocationService getService() {
            return LocationService.this;
        }
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        startToStoreLocation = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (startToStoreLocation){
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(serviceCallbacks != null)
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        saveLocation(serviceCallbacks.getLocation());
                                    }
                                }).start();
                        }
                    }, 5000);
                }
            }
        }).start();
        return binder;
    }

    public void saveLocation(String location){
        try {
            OutputStreamWriter out = new OutputStreamWriter(openFileOutput("location.txt", MODE_APPEND));
            out.write(location);
            out.write('\n');
            out.close();
        } catch (java.io.IOException e) {
        }
    }

    public void setCallbacks(ServiceCallBacks callbacks) {
        serviceCallbacks = callbacks;
    }
}
