package com.android.locationtest.interfaces;

public interface ServiceCallBacks {
    String getLocation();
}
