package com.android.locationtest;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Login extends AppCompatActivity {
    SharedPreferences shp;
    SharedPreferences.Editor shpEditor;
    EditText edtUserName, edtMobileNumber;
    Button btnLogin;
    TextView txtInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        edtUserName = findViewById(R.id.edtUserName);
        edtMobileNumber = findViewById(R.id.edtMobileNumber);
        edtUserName.setText("");
        edtMobileNumber.setText("");
        btnLogin = findViewById(R.id.btnLogin);
        txtInfo = findViewById(R.id.txtInfo);
        shp = getSharedPreferences("myPreferences", MODE_PRIVATE);
        CheckLogin();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtUserName.getText().toString().equals("") || edtMobileNumber.getText().toString().equals(""))
                    txtInfo.setText("Please insert username and mobile number");
                else
                    DoLogin(edtUserName.getText().toString(), edtMobileNumber.getText().toString());
            }
        });
    }

    public void CheckLogin() {
        if (shp == null)
            shp = getSharedPreferences("myPreferences", MODE_PRIVATE);

        String mobile_number = shp.getString("mobile_number", "");

        if (mobile_number != null && !mobile_number.equals("")) {
            Intent i = new Intent(Login.this, MainActivity.class);
            startActivity(i);
            finish();
        }
    }

    public void DoLogin(String username, String mobile_number) {
        try {
//            if (password.equals("Android3am")) {
                if (shp == null)
                    shp = getSharedPreferences("myPreferences", MODE_PRIVATE);

                shpEditor = shp.edit();
                shpEditor.putString("mobile_number", mobile_number);
                shpEditor.commit();

                Intent i = new Intent(Login.this, MainActivity.class);
                startActivity(i);
                finish();
//            } else
//                txtInfo.setText("Invalid Credentails");
        } catch (Exception ex) {
            txtInfo.setText(ex.getMessage().toString());
        }
    }
}